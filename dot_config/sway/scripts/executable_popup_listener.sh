#!/usr/bin/env bash

# From https://gist.github.com/sebastiancarlos/5d67ee43cf0e2afa0614e7c9d7af0145

# This script listens for new windows and makes them floating if it thinks they
# are popups. Meant to be used when regular i3 criteria are not enough to
# identify popup windows. It uses hardcoded dimensions.

# print usage on --help/-h
if [[ "$1" == "--help" || "$1" == "-h" ]]; then
    echo "Usage: $0"
    echo "This script listens for new windows and makes them floating if it thinks they"
    echo "are popups. Meant to be used when regular i3 criteria are not enough to"
    echo "identify popup windows. It uses hardcoded dimensions."

    echo "Meant to be called from sway config:"
    echo "exec <script_location>"
    exit 0
fi

# array of dimensions to turn into floating windows
declare -a popup_dimensions=(
    "360x96"  # chromium notifications
    "360x119" # chromium notifications
)

# for every new window
swaymsg --monitor -t subscribe '["window"]' |
    jq --raw-output --unbuffered 'select(.change == "new") | .container | {id, geometry} | "\(.id):\(.geometry.width)x\(.geometry.height)"' |
    while read -r new_window; do
        id=${new_window%%:*}
        dimensions=${new_window#*:}

        # if dimensions match, make floating
        for popup_dimension in "${popup_dimensions[@]}"; do
            if [ "$popup_dimension" == "$dimensions" ]; then
                swaymsg [con_id=$id] floating enable
                break
            fi
        done
    done
