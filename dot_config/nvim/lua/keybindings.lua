local map = require("utils").map
local wrap = function(func, ...)
  local args = {...}
  return function()
    func(table.unpack(args))
  end
end

vim.g.mapleader = ","

-- wrap
map("n", "<Leader>w", ":set wrap! linebreak!<cr>")
map("n", "j", "gj")
map("n", "k", "gk")

-- navigation
--- behave like other capitals
map("n", "Y", "y$")
--- keeping it centered
map("n", "n", "nzzzv")
map("n", "N", "Nzzzv")
map("n", "J", "mzJ`z")
--- moving text
map("v", "J", ":m '>+1<CR>gv=gv")
map("v", "K", ":m '<-2<CR>gv=gv")
map("n", "<leader>k", ":m .-2<CR>==")
map("n", "<leader>j", ":m .+1<CR>==")

-- telescope
local builtin = require('telescope.builtin')
map('n', '<leader>ff', builtin.find_files, {})
map('n', '<leader>fg', builtin.live_grep, {})
map('n', '<leader>fb', builtin.buffers, {})
map('n', '<leader>fh', builtin.help_tags, {})
map('n', '<leader>ft', builtin.treesitter, {})
map('n', '<leader>fc', builtin.commands, {})
map("n", "<Leader>fr", "<cmd>Telescope bibtex<cr>")

--- quicklist
map("n", "<leader>qn", "<cmd>:cnext<cr>")
map("n", "<leader>qp", "<cmd>:cprev<cr>")
map("n", "<leader>qo", "<cmd>:copen<cr>")

-- lua tree
map("n", "<Leader>tt", "<cmd>NvimTreeToggle<cr>")
map("n", "<Leader>tf", "<cmd>NvimTreeFindFileToggle<cr>")
map("n", "<Leader>tr", "<cmd>NvimTreeRefresh<cr>")

-- language server
local buf = vim.lsp.buf
local diag = vim.diagnostic
map("n", "<Leader>vd", buf.definition)
map("n", "<Leader>vi", buf.implementation)
map("n", "<Leader>vsh", buf.signature_help)
map("n", "<Leader>vrr", buf.references)
map("n", "<Leader>vrn", buf.rename)
map("n", "<Leader>vh", buf.hover)
map("n", "<Leader>vca", buf.code_action)
map("n", "<Leader>vsd", wrap(diag.open_float, {scope='line'}))
map("n", "<Leader>vn", diag.goto_next)
map("n", "<Leader>vp", diag.goto_prev)
map("n", "<Leader>vf", "<cmd>Format<CR>")


-- neogit
map("n", "<Leader>go", "<cmd>Neogit<CR>")
map("n", "<Leader>gc", "<cmd>Neogit commit<CR>")
map("n", "<Leader>gws", require('telescope').extensions.git_worktree.git_worktrees)
map("n", "<Leader>gwc", require('telescope').extensions.git_worktree.create_git_worktree)

--- zen-mode
map("n", "<leader>z", "<cmd>ZenMode<cr>")

-- barbar
local opts = {
  silent = true
}
--- Move to previous/next
map('n', '<A-,>', '<Cmd>BufferPrevious<CR>', opts)
map('n', '<A-.>', '<Cmd>BufferNext<CR>', opts)
--- Re-order to previous/next
map('n', '<A-<>', '<Cmd>BufferMovePrevious<CR>', opts)
map('n', '<A->>', '<Cmd>BufferMoveNext<CR>', opts)
--- Goto buffer in position...
map('n', '<A-1>', '<Cmd>BufferGoto 1<CR>', opts)
map('n', '<A-2>', '<Cmd>BufferGoto 2<CR>', opts)
map('n', '<A-3>', '<Cmd>BufferGoto 3<CR>', opts)
map('n', '<A-4>', '<Cmd>BufferGoto 4<CR>', opts)
map('n', '<A-5>', '<Cmd>BufferGoto 5<CR>', opts)
map('n', '<A-6>', '<Cmd>BufferGoto 6<CR>', opts)
map('n', '<A-7>', '<Cmd>BufferGoto 7<CR>', opts)
map('n', '<A-8>', '<Cmd>BufferGoto 8<CR>', opts)
map('n', '<A-9>', '<Cmd>BufferGoto 9<CR>', opts)
map('n', '<A-0>', '<Cmd>BufferLast<CR>', opts)
--- Pin/unpin buffer
map('n', '<A-p>', '<Cmd>BufferPin<CR>', opts)
--- Close buffer
map('n', '<A-c>', '<Cmd>BufferClose<CR>', opts)
--- Wipeout buffer
---                 :BufferWipeout
--- Close commands
---                 :BufferCloseAllButCurrent
---                 :BufferCloseAllButPinned
---                 :BufferCloseAllButCurrentOrPinned
---                 :BufferCloseBuffersLeft
---                 :BufferCloseBuffersRight
--- Magic buffer-picking mode
map('n', '<C-p>', '<Cmd>BufferPick<CR>', opts)
--- Sort automatically by...
map('n', '<Space>bb', '<Cmd>BufferOrderByBufferNumber<CR>', opts)
map('n', '<Space>bd', '<Cmd>BufferOrderByDirectory<CR>', opts)
map('n', '<Space>bl', '<Cmd>BufferOrderByLanguage<CR>', opts)
map('n', '<Space>bw', '<Cmd>BufferOrderByWindowNumber<CR>', opts)

-- Knap preview
local knap = require("knap")
--- F5 processes the document once, and refreshes the view
map({ 'n', 'v', 'i' }, '<F5>', knap.process_once)
--- F6 closes the viewer application, and allows settings to be reset
map({ 'n', 'v', 'i' }, '<F6>', knap.close_viewer)
--- F7 toggles the auto-processing on and off
map({ 'n', 'v', 'i' }, '<F7>', knap.toggle_autopreviewing)
--- F8 invokes a SyncTeX forward search, or similar, where appropriate
map({ 'n', 'v', 'i' }, '<F8>', knap.forward_jump)

-- Trouble
map("n", "<leader>xx", "<cmd>TroubleToggle<cr>",
  { silent = true, noremap = true }
)
map("n", "<leader>xw", "<cmd>TroubleToggle workspace_diagnostics<cr>",
  { silent = true, noremap = true }
)
map("n", "<leader>xd", "<cmd>TroubleToggle document_diagnostics<cr>",
  { silent = true, noremap = true }
)
map("n", "<leader>xl", "<cmd>TroubleToggle loclist<cr>",
  { silent = true, noremap = true }
)
map("n", "<leader>xq", "<cmd>TroubleToggle quickfix<cr>",
  { silent = true, noremap = true }
)
map("n", "gR", "<cmd>TroubleToggle lsp_references<cr>",
  { silent = true, noremap = true }
)
