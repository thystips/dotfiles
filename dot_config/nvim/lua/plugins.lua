local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
        vim.cmd [[packadd packer.nvim]]
        return true
    end
    return false
end

local packer_bootstrap = ensure_packer()

return require("packer").startup { function(use)
    -- Packer can manage itself
    use "wbthomason/packer.nvim"

    use 'folke/lsp-colors.nvim'

    use "rcarriga/nvim-notify"

    use {
        "williamboman/mason.nvim", "williamboman/mason-lspconfig.nvim", "neovim/nvim-lspconfig",
        wants = { "cmp-nvim-lsp", "nvim-lsp-installer", "lsp_signature.nvim" },
        requires = {
            "williamboman/nvim-lsp-installer",
            "ray-x/lsp_signature.nvim",
            "hrsh7th/cmp-nvim-lsp",
            "folke/neodev.nvim"
        },
        after = { "cmp-nvim-lsp" },
        config = function()
            require('plugins/mason').setup()
            require("neodev").setup()
            require("plugins.lsp").setup()
        end
    }

    use {
        -- vscode-like pictograms for neovim lsp completion items Topics
        "onsails/lspkind-nvim",
        config = [[ require('plugins/lspkind') ]]
    }
    --

    use {
        -- Utility functions for getting diagnostic status and progress messages from LSP servers, for use in the Neovim statusline
        "nvim-lua/lsp-status.nvim",
        config = [[ require('plugins/lspstatus') ]]
    }
    use {
        "zbirenbaum/copilot.lua",
        cmd = "Copilot",
        event = "InsertEnter",
        config = [[ require('plugins/copilot') ]]
    }

    use {
        "zbirenbaum/copilot-cmp",
        after = { "copilot.lua" },
        config = [[ require('plugins/copilot_cmp') ]]
    }

    use {
        "petertriho/cmp-git",
        after = { "nvim-cmp" },
        requires = "nvim-lua/plenary.nvim",
        config = [[ require('plugins/cmp_git') ]]
    }
    use {
        "hrsh7th/cmp-nvim-lsp",
        after = "nvim-cmp"
    }
    use {
        "hrsh7th/cmp-nvim-lua",
        after = "nvim-cmp"
    }
    use {
        "hrsh7th/cmp-buffer",
        after = "nvim-cmp"
    }
    use {
        "hrsh7th/cmp-path",
        after = "nvim-cmp"
    }
    use {
        "hrsh7th/cmp-calc",
        after = "nvim-cmp"
    }
    use {
        "saadparwaiz1/cmp_luasnip",
        after = "nvim-cmp"
    }

    use {
        -- A completion plugin for neovim coded in Lua.
        "hrsh7th/nvim-cmp",
        after = { "copilot-cmp" },
        requires = { "hrsh7th/cmp-nvim-lsp", "hrsh7th/cmp-nvim-lua", "hrsh7th/cmp-buffer", "hrsh7th/cmp-path",
            "hrsh7th/cmp-calc", "saadparwaiz1/cmp_luasnip", "copilot-cmp" },
        config = [[ require('plugins/cmp') ]]
    }

    use { "nvim-telescope/telescope-project.nvim" }

    use {
        "nvim-telescope/telescope-fzf-native.nvim",
        run = "make"
    }

    use { "cljoly/telescope-repo.nvim" }

    use {
        "nvim-telescope/telescope-file-browser.nvim",
        wants = { "telescope.nvim" }
    }

    use {
        "nvim-telescope/telescope-bibtex.nvim",
        wants = { "telescope.nvim" }
    }

    use {
        "LinArcX/telescope-command-palette.nvim",
        wants = { "telescope.nvim" }
    }

    use {
        "nvim-telescope/telescope-z.nvim",
        wants = { "telescope.nvim" }
    }

    use {
        "benfowler/telescope-luasnip.nvim",
        module = "telescope._extensions.luasnip",
    }

    use { "xiyaowong/telescope-emoji.nvim" }

    use {
        "nvim-telescope/telescope.nvim",
        requires = { "nvim-lua/plenary.nvim", "BurntSushi/ripgrep" },
        config = [[ require('plugins/telescope') ]]
    }

    use {
        "dhananjaylatkar/cscope_maps.nvim",
        requires = {
            "folke/which-key.nvim",          -- optional [for whichkey hints]
            "nvim-telescope/telescope.nvim", -- optional [for picker="telescope"]
            "nvim-tree/nvim-web-devicons",   -- optional [for devicons in telescope]
        },
        config = [[ require('plugins/cscope_maps') ]]
    }

    use {
        -- Snippet Engine for Neovim written in Lua.
        "L3MON4D3/LuaSnip",
        requires = {
            "rafamadriz/friendly-snippets" -- Snippets collection for a set of different programming languages for faster development.
        },
        config = [[ require('plugins/luasnip') ]]
    }

    --  colorscheme for (neo)vim
    -- use { 'projekt0n/github-nvim-theme', branch = '0.0.x' }
    use {
        "catppuccin/nvim",
        as = "catppuccin",
        -- config = [[ require('plugins/catppuccin') ]]
    }

    use { "vimpostor/vim-lumen" }

    use {
        -- Nvim Treesitter configurations and abstraction layer
        "nvim-treesitter/nvim-treesitter",
        run = ":TSUpdate",
        requires = { "windwp/nvim-ts-autotag", "p00f/nvim-ts-rainbow" },
        config = [[ require('plugins/treesitter') ]]
    }

    use {
        "IndianBoy42/tree-sitter-just",
        requires = { "nvim-treesitter/nvim-treesitter" },
        config = [[ require('tree-sitter-just').setup({}) ]]
    }

    use {
        "lukas-reineke/indent-blankline.nvim",
        config = [[ require('plugins/blankline') ]]
    }

    use { "tpope/vim-eunuch" }

    use {
        "nvim-lualine/lualine.nvim",
        requires = {
            "kyazdani42/nvim-web-devicons",
            opt = true
        },
        config = [[ require('plugins/lualine') ]]
    }

    use {
        "TimUntersberger/neogit",
        requires = { "nvim-lua/plenary.nvim" },
        config = [[ require('plugins/neogit') ]]
    }

    use {
        "nvim-tree/nvim-tree.lua",
        requires = "nvim-tree/nvim-web-devicons",
        config = [[ require('plugins/nvim-tree') ]]
    }

    use {
        "folke/zen-mode.nvim",
        config = [[ require('plugins/zen-mode') ]]
    }

    use {
        "ThePrimeagen/git-worktree.nvim",
        config = [[ require('plugins/git-worktree') ]]
    }

    use {
        "mhartington/formatter.nvim",
        config = [[ require('plugins/formatter') ]]
    }

    use { "kmonad/kmonad-vim" }

    use { "tpope/vim-obsession" }

    use { "lambdalisue/suda.vim" }

    -- Tools
    use { "echasnovski/mini.nvim" }
    use {
        "folke/which-key.nvim",
        config = [[ require('plugins/which-key') ]]
    }

    -- lint
    use {
        "folke/trouble.nvim",
        requires = "nvim-tree/nvim-web-devicons",
        config = function()
            require("trouble").setup {}
        end
    }
    use {
        "folke/todo-comments.nvim",
        requires = "nvim-lua/plenary.nvim",
        config = function()
            require("todo-comments").setup {}
        end
    }


    -- Git
    use { "tpope/vim-fugitive" }

    -- Comment
    use { "preservim/nerdcommenter" }

    -- Ansible
    use { "pearofducks/ansible-vim" }

    -- Chezmoi
    use { "alker0/chezmoi.vim" }

    -- Schema
    use { "jiangmiao/auto-pairs" }
    use { "editorconfig/editorconfig-vim" }

    -- Terminal
    use {
        "jghauser/kitty-runner.nvim",
        config = function()
            require("kitty-runner").setup()
        end
    }
    use({
        "aserowy/tmux.nvim",
        config = [[ require('plugins/tmux') ]]
    })

    -- Preview
    use { "gennaro-tedesco/nvim-peekup" }
    use { "frabjous/knap" }

    -- Clipboard
    use {
        "AckslD/nvim-neoclip.lua",
        requires = { {
            'kkharji/sqlite.lua',
            module = 'sqlite'
        },
            { 'nvim-telescope/telescope.nvim' }
        },
        config = function()
            require('neoclip').setup()
            require('telescope').load_extension('neoclip')
        end
    }

    -- Interface
    use {
        "nvim-tree/nvim-web-devicons",
        config = function()
            require 'nvim-web-devicons'.setup()
        end
    }
    use {
        'glepnir/dashboard-nvim',
        event = 'VimEnter',
        config = [[ require('plugins/dashboard') ]],
        requires = { 'nvim-tree/nvim-web-devicons' }
    }
    use {
        "romgrk/barbar.nvim",
        requires = { "nvim-web-devicons", "nvim-tree.lua" },
        config = [[ require('plugins/barbar') ]]
    }

    if packer_bootstrap then
        require("packer").sync()
    end
end }
