local M = {}

function M.setup(lspconfig)
    lspconfig.lua_ls.setup({
        settings = {
            Lua = {
                completion = {
                    callSnippet = "Replace"
                }
            }
        }
    })
end

return M
