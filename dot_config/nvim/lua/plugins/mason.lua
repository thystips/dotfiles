local M = {}

function M.setup()
    require("mason").setup()
    require("mason-lspconfig").setup {
        ensure_installed = { "lua_ls"},
        automatic_installation = true
    }
    require("mason-lspconfig").setup_handlers {
        function (server_name)
            require("lspconfig")[server_name].setup {}
        end
    }

end

return M
