require('dashboard').setup {
    theme = "hyper",
    config = {
        week_header = {
            enable = true
        },
        shortcut = { {
            desc = " Update",
            group = "@property",
            action = "PackerSync",
            key = "u"
        }, {
            desc = " Files",
            group = "Label",
            action = "Telescope find_files",
            key = "f"
        }, {
            desc = " dotfiles",
            group = "Number",
            action = ":lua require('plugins.telescope.functions').find_configs()",
            key = "d"
        } }
    }

}
