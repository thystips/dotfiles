local t = require("telescope")
t.load_extension("project")
t.load_extension("notify")
t.load_extension("repo")
t.load_extension("emoji")

local lst = require("telescope").extensions.luasnip
local luasnip = require('luasnip')
local actions = require("telescope.actions")
local trouble = require("trouble.providers.telescope")
local builtin = require('telescope.builtin')

t.setup {
    defaults = {
        file_ignore_patterns = { "node_modules", ".git", "dist", ".next", "out", "pnpm-lock.yaml" },
        vimgrep_arguments = { "rg", "--color=never", "--no-heading", "--with-filename", "--line-number", "--column",
            "--hidden", "--iglob", "!yarn.lock", "--smart-case", "-u" },
        -- Default configuration for telescope goes here:
        -- config_key = value,
        mappings = {
            i = {
                -- map actions.which_key to <C-h> (default: <C-/>)
                -- actions.which_key shows the mappings for your picker,
                -- e.g. git_{create, delete, ...}_branch for the git_branches picker
                ["<C-h>"] = "which_key"
            },
        }
    },
    pickers = {
        find_files = {
            find_command = { "rg", "--files", "--hidden", "--ignore-file", ".next", "--ignore-file", "out",
                "--ignore-file", "pnpm-lock.yaml" }
        },
        file_browser = {
            hidden = true
        }
        -- picker_name = {
        --   picker_config_key = value,
        --   ...
        -- }
        -- Now the picker_config_key will be applied every time you call this
        -- builtin picker
    },
    extensions = {
        project = {
            fzf = {
                fuzzy = true,                   -- false will only do exact matching
                override_generic_sorter = true, -- override the generic sorter
                override_file_sorter = true,    -- override the file sorter
                case_mode = "smart_case"        -- or "ignore_case" or "respect_case"
                -- the default case_mode is "smart_case"
            },
            base_dirs = { {
                path = "~/Sources",
                max_depth = 2
            } },
            hidden_files = true
        },
        bibtex = {
            format = 'plain'
        },
        repo = {
            settings = {
                auto_lcd = true
            }
        },
        emoji = {
            action = function(emoji)
                -- argument emoji is a table.
                -- {name="", value="", cagegory="", description=""}

                vim.fn.setreg("*", emoji.value)
                print([[Press p or "*p to paste this emoji]] .. emoji.value)

                -- insert emoji when picked
                -- vim.api.nvim_put({ emoji.value }, 'c', false, true)
            end,
        },
        command_palette = {
            { "File",
                { "entire selection (C-a)",  ':call feedkeys("GVgg")' },
                { "save current file (C-s)", ':w' },
                { "save all files (C-A-s)",  ':wa' },
                { "quit (C-q)",              ':qa' },
                { "file browser (C-i)",
                    ":lua require('plugins.telescope.functions').extensions.file_browser.file_browser()", 1 },
                { "search word (A-w)", ":lua require('telescope.builtin').live_grep()",               1 },
                { "git files (A-f)",   ":lua require('telescope.builtin').git_files()",               1 },
                { "files (C-f)",       ":lua require('telescope.builtin').find_files()",              1 },
                { "Project Files",     ":lua require('plugins.telescope.functions').project_files()", 1 },
            },
            { "Terminal",
                { "Vertical Right", ":vsp | terminal",                                        1 },
                { "Edit dotfiles",  ":lua require('plugins.telescope.functions').dotfiles()", 1 },
            },
            { "Help",
                { "tips",            ":help tips" },
                { "cheatsheet",      ":help index" },
                { "tutorial",        ":help tutor" },
                { "summary",         ":help summary" },
                { "quick reference", ":help quickref" },
                { "search help(F1)", ":lua require('telescope.builtin').help_tags()", 1 },
            },
            { "Notes",
                { "Browse Notes",      ":lua require('plugins.telescope.functions').browse_notes()", 1 },
                { "Find Notes",        ":lua require('plugins.telescope.functions').find_notes()",   1 },
                { "Search/Grep Notes", ":lua require('plugins.telescope.functions').grep_notes()",   1 },
            },
            { "Toggle",
                { "cursor line",         ":set cursorline!" },
                { "cursor column",       ":set cursorcolumn!" },
                { "spell checker",       ":set spell!" },
                { "relative number",     ":set relativenumber!" },
                { "search highlighting", ":set hlsearch!" },
                { "Colorizer",           ":ColorToggle" },
            },
            { "NeoVim",
                { "reload vimrc",              ":source $MYVIMRC" },
                { 'check health',              ":checkhealth" },
                { "jumps (Alt-j)",             ":lua require('telescope.builtin').jumplist()" },
                { "commands",                  ":lua require('telescope.builtin').commands()" },
                { "command history",           ":lua require('telescope.builtin').command_history()" },
                { "registers (A-e)",           ":lua require('telescope.builtin').registers()" },
                { "colorshceme",               ":lua require('telescope.builtin').colorscheme()",           1 },
                { "vim options",               ":lua require('telescope.builtin').vim_options()" },
                { "keymaps",                   ":lua require('telescope.builtin').keymaps()" },
                { "buffers",                   ":Telescope buffers" },
                { "search history (C-h)",      ":lua require('telescope.builtin').search_history()" },
                { "paste mode",                ':set paste!' },
                { 'cursor line',               ':set cursorline!' },
                { 'cursor column',             ':set cursorcolumn!' },
                { "spell checker",             ':set spell!' },
                { "relative number",           ':set relativenumber!' },
                { "search highlighting (F12)", ':set hlsearch!' },
                { "Search TODOS",              ":lua require('plugins.telescope.functions').search_todos()" },
            }
        },
        luasnip = {
            search = function(entry)
                return lst.filter_null(entry.context.trigger) .. " " ..
                    lst.filter_null(entry.context.name) .. " " ..
                    entry.ft .. " " ..
                    lst.filter_description(entry.context.name, entry.context.description) ..
                    lst.get_docstring(luasnip, entry.ft, entry.context)[1]
            end
        },

    }
}

t.load_extension('fzf')
t.load_extension('bibtex')
t.load_extension('file_browser')
t.load_extension('command_palette')
t.load_extension('z')
t.load_extension('luasnip')
