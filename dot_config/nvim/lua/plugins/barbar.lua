-- Set barbar's options
require 'bufferline'.setup {
    -- Enable/disable auto-hiding the tab bar when there is a single buffer
    auto_hide = false,

    -- Excludes buffers from the tabline
    exclude_ft = { 'javascript' },
    exclude_name = { 'package.json' }
}

-- nvim-tree integration
local nvim_tree_events = require('nvim-tree.events')
local bufferline_api = require('bufferline.api')

local function get_tree_size()
    return require 'nvim-tree.view'.View.width
end

nvim_tree_events.subscribe('TreeOpen', function()
    bufferline_api.set_offset(get_tree_size())
end)

nvim_tree_events.subscribe('Resize', function()
    bufferline_api.set_offset(get_tree_size())
end)

nvim_tree_events.subscribe('TreeClose', function()
    bufferline_api.set_offset(0)
end)
